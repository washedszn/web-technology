const express = require('express');
const {StatusCodes} = require('http-status-codes');
const Database = require('../database/database');
const database = new Database('accounts');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const router = express.Router();

router.post('/', (req, res) => {
    // console.log(req.body.username + '\n' + req.body.password);
    const {username, password} = req.body;

    if (username && password) {
        const token = login(username, password);
        if (token) {
            res.send({token: token});
        } else {
            res.status(StatusCodes.UNAUTHORIZED).send('Username and/or password incorrect!');
        }
    } else {
        res.status(StatusCodes.UNAUTHORIZED).send('Required parameters missing!');
    }
});

const login = (username, password) => {
    const account = database.getByName(username);
    if (account) {
        const result = bcrypt.compareSync(password, account.hash);
        if (result) {
            // Send a token
            return jwt.sign({username: account.username, roles: account.roles}, account.secret);
        }
    }
    return false;
};

module.exports = router;