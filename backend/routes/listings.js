const express = require('express');
const router = express.Router();
const Database = require('../database/database');
const database = new Database('listings');
const isLoggedIn = require('../middleware/is-logged-in');
const isAdmin = require('../middleware/is-admin');
const {StatusCodes} = require("http-status-codes");

// here we put our listings routes
router.get('/', (req, res) => {
    let listings = database.get(req.body.id)
    if (!listings) return res.status(500).send();
    return res.status(200).json(listings).send();
});

router.get('/:id', (req, res) => {
    let listing = database.get(req.params.id);
    if (!listing) return res.status(500).send('item with such id does not exist');
    return res.status(200).json(listing).send();
});

router.post('/', isLoggedIn, (req, res) => {
        database.create(req.body);
        res.status(StatusCodes.CREATED).send('Item was created');
});

router.put('/', (req, res) => {
    let listing = req.body;
    database.edit(listing);
    res.status(200).send();
});

router.delete('/:id', isLoggedIn, isAdmin, (req, res) => {
    if (!database.get(req.params.id)) {
        console.log('item with such id does not exist')
        res.status(500).send('item with such id does not exist');
    } else {
        database.remove(req.params.id);
        console.log('item was deleted');
        res.status(204).send('item was deleted');
    }
});

module.exports = router;