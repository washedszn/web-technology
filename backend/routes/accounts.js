const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt')
const { v4: uuidv4 } = require('uuid');
const isLoggedIn = require('../middleware/is-logged-in');
const Database = require('../database/database');
const database = new Database('accounts');

// here we put our accounts routes
router.get('/', (req, res) => {
    let accounts = database.get(req.body.id)
    if (!accounts) return res.status(500).send();
    return res.status(200).json(accounts).send();
});

router.get('/:id', (req, res) => {
    let account = database.get(req.params.id);
    if (!account) return res.status(500).send('user with such id does not exist');
    return res.status(200).json(account).send();
});

router.post('/', (req, res) => {
    if (!database.getByName(req.body.username)) {
        // create accounts and hash password
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(req.body.password, salt, function (err, hash) {
                let newAccount = {
                    username: req.body.username,
                    hash: hash,
                    secret: uuidv4(),
                    roles: ['admin', 'user']
                }
                database.create(newAccount);
            });
        });
        console.log("user created");
        res.status(200).send("user created");
    } else {
        console.log('user with such username already exists');
        res.status(500).send('user with such username already exists');
    }
});

router.post('/login', (req, res) => {
    let account = database.getByName(req.body.username);
    if (!account) return res.status(500).send();
    bcrypt.compare(req.body.password, account.hash, (err, data) => {
        if (err) return console.log(err);
        if (!data) return console.log('password is incorrect'), res.send('password is incorrect');
        res.status(200).send('user is logged in'), console.log('user is logged in');
    });
})

router.put('/', isLoggedIn, (req, res) => {
    let account = req.body;
    database.edit(account);
    res.status(200).send();
});

router.delete('/:id', isLoggedIn, (req, res) => {
    if (!database.get(req.params.id)) {
        console.log('user with such id does not exist')
        res.status(500).send('user with such id does not exist');
    } else {
        database.remove(req.params.id);
        console.log('user was deleted');
        res.status(204).send('user was deleted');
    }
});

module.exports = router;