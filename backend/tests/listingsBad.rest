### GET request with a list of all accounts [BAD]
GET http://localhost:8080/listing

### GET request with one account with a specified id [BAD]
GET http://localhost:8080/listings/cfd53dbf-e9a9-4d4b-89a6-661db2d35e32

### POST request with one new listing created [BAD]
POST http://localhost:8080/listing
Content-Type: application/json

{
  "test" : true
}

### PUT request with one eddited listing by id [BAD]
PUT http://localhost:8080/listings/cfd53dbf-e9a9-4d4b-89a6-661db2d35e31
Content-Type: application/json

{
  "test": true
}
### DELETE request with one deleted listing by id [BAD]
DELETE http://localhost:8080/listings/919d2944-55d7-4836-bcbc-ab6dba5d27b4