const data = require('./database.json');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

class Database {
    constructor(type) {
        this.type = type;
    }

    writeData() {
        fs.writeFile('./database/database.json', JSON.stringify(data, null, 4), (err, data) => {
            return !err;
        })
    }
    
    create(obj) {
        obj.id = uuidv4();
        data[this.type].push(obj)
        return this.writeData();
    }
    
    edit(obj) {
        let selected = data[this.type].find(e => e.id === obj.id);
        if (!selected) return false;
        let i = data[this.type].indexOf(selected);
        data[this.type][i] = obj;
        return this.writeData();
    }
    
    remove(id) {
        data[this.type] = data[this.type].filter(e => e.id !== id);
        return this.writeData();
    }
    
    get(id) {
        return id ? data[this.type].find(e => e.id === id) : data[this.type];
    }

    getByName(username) {
        return username ? data[this.type].find(e => e.username === username) : data[this.type];
    }
}

module.exports = Database;