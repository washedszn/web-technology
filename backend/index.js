const express = require('express');
const app = express();

// routes
const listings = require('./routes/listings');
const accounts = require('./routes/accounts');
const credentials = require('./routes/credentials');

const port = 8080;

app.use(express.json());

app.use('/listings', listings);
app.use('/accounts', accounts);
app.use('/credentials', credentials);

app.get('/test', (req, res) => {
    console.log('working')
})

app.listen(port, () => {
    console.log('App listening on port ' + port);
})