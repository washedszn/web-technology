## Endpoint Documentation

### GET /listings

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | Not required | path | Not required |

| Status Code: | Description: |
|--|--|
| 200 | Returns all listings |
| 500 | Internal server error whilst retrieving listings

### GET /listings/{id}

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | `id` | Path | Filtered on `id` |

| Status Code: | Description: |
|--|--|
| 200 | Returns listing that corresponds with the provided `id` |
| 204 | Server processed request correctly, although no listing was found with the provided `id`
| 500 | Internal server error whilst retreiving listing |

### GET /accounts

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | Not required | Path | Not required |

| Status Code: | Description: |
|--|--|
| 200 | Returns all accounts  |
| 500 | Internal server error whilst retrieving accounts |

### GET /accounts/{id}

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | `id` | Path | Filtered on `id` |

| Status Code: | Description: |
|--|--|
| 200 | Returns account that corresponds with the provided `id` |
| 500 | Internal server error whilst retrieving account |

### GET /users/{username}/listings

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | `username` | Path | Filtered on `username` |

| Status Code: | Description: |
|--|--|
| 200 | Returns all listings that the account `username` has listed |
| 500 | Internal server error whilst retrieving accounts listings |

### POST /listings

*Form validation will take place on the frontend, so the server does not need to handle that*

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | Not required | Path | Not required |

| Status Code: | Description: |
|--|--|
| 200 | Server successfully created new listing |
| 500 | Internal server error whilst trying to create new listing |

```js
{
    startDate: Date,
    endDate: Date,
    sellerId: String,
    item: {
        name: String,
        quality: String,
        price: Number,
        quantity: Number,
        pictures: Array
    }
}
```

### POST /accounts

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | Not required | Path | Not required |

| Status Code: | Description: |
|--|--|
| 200 | Server successfully created new account |
| 500 | Internal server error whilst trying to create new account |

```js
{ 
    firstName: String,
    lastName: String,
    age: Number,
    address: {
       street: String,
       number: Number,
       extra: String,
       zip: String,
       city: String
    }  
} 
```

### POST /listings/bids

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | Not required | Path | Not required |

| Status Code: | Description: |
|--|--|
| 200 | Server successfully adds new bid to listing |
| 400 | Bad request due to bid being lower than current price of listing |
| 500 | Internal server error whilst trying to add bid to item |

### POST /listings/buys

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | Not required | Path | Not required |

| Status Code: | Description: |
|--|--|
| 200 | Server successfully processes request to buy listing |
| 500 | Internal server error whilst trying to complete purchase of listing |

### PUT /listings/{id}

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | `id` | Path | Filtered on `id` |

| Status Code: | Description: |
|--|--|
| 204 | Server successfuly edits listing that corresponds with the given `id` |
| 500 | Internal server error whilst trying to edit listing |

### PUT /accounts/{id}

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | `id` | Path | Filtered on `id` |

| Status Code: | Description: |
|--|--|
| 204 | Server successfuly edits account that corresponds with the given `id` |
| 500 | Internal server error whilst trying to edit account |

### DELETE /listings/{id}

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | `id` | Path | Filtered on `id` |

| Status Code: | Description: |
|--|--|
| 204 | Server successfully deletes listing that corresponds with the provided `id` |
| 500 | Internal server error whilst trying to delete listing |

### DELETE /accounts/{id}

| Parameters: | Name: | Type: | Description: |
|--|--|--|--|
| Not required | `id` | Path | Filtered on `id` |

| Status Code: | Description: |
|--|--|
| 204 | Server successfully deletes account that corresponds with the provided `id` |
| 500 | Internal server error whilst trying to delete account |