const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Database = require('../database/database');
const database = new Database('accounts');
const {StatusCodes} = require('http-status-codes');
const {log} = require("nodemon/lib/utils");

// NEW CODE
const isLoggedIn = (req, res, next) => {
    const token = getTokenFromRequest(req);

    if (token) {
        const payload = verifyToken(token);

        if (payload) {
            req.account = payload;
            return next();
        }
    }

    return res.status(StatusCodes.UNAUTHORIZED).send('Something is wrong with your credentials!');
}

const verifyToken = (token) => {
    try {
        const tokenPayload = jwt.decode(token);
        console.log('Token payload', tokenPayload);

        if (tokenPayload) {
            const account = database.getByName(tokenPayload.username);
            try {
                return jwt.verify(token, account.secret);
            } catch (e) {
                return false;
            }
        }
    } catch (e) {
        return false;
    }
};

const getTokenFromRequest = (req) => {
    const authHeader = req.headers['authorization']
    if (authHeader) return authHeader.split(' ')[1];
    return false;
}

module.exports = isLoggedIn;