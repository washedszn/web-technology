const {StatusCodes} = require('http-status-codes');

const isAdmin = (req, res, next) => {
    if (req.account.roles.includes('admin')) {
        return next();
    }

    res.status(StatusCodes.UNAUTHORIZED).send('You need admin privileges for this!');
};

module.exports = isAdmin;